document.addEventListener("DOMContentLoaded", function() {
    
    document.getElementById("formularioEnvio").addEventListener("submit", function(event){
        event.preventDefault();

        var url = document.getElementById("formularioEnvio").action;
        var metodo = document.getElementById("formularioEnvio").method;
        var formSelector= document.getElementById("formularioEnvio");
    
        const formData = new FormData(formSelector)

        fetch(url, {
            method: metodo, // 'GET', 'PUT', 'DELETE', etc.
            body: formData  // a FormData will automatically set the 'Content-Type'
        })
        .then(function(response){
            console.log(response);
            alert("Problem!!");
            var element1 = document.getElementsByClassName("formulario")[0];
            element1.classList.add("esconderFormulario");
            var element2 = document.getElementsByClassName("karma")[0];
            element2.classList.add("formularioEnviado");
            })
        .catch(function(response) {
            console.log("Something went wrong")
        });
    })

 });



