Proyecto para la app Maleteo en PHP Symfony.

Landing page con:
  - Formulario para registro de usuarios.
  - Consulta de usuarios inscritos y sus datos.
  
Pasos para el despliegue de la aplicación:
- docker-compose up -d en el directorio del proyecto.
- Ejecutar localhost:8000 en el navegador.