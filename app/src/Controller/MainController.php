<?php


namespace App\Controller;

use App\Entity\Usuario;
use App\Form\UsuarioFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name = "homepage")
     */
    public function index()
    {
        return $this->render(
            'base.html.twig'
        );
    }

    /**
     * @Route("/usuarios", name="listar_usuarios")
     */

    public function listarUsuarios(EntityManagerInterface $em)
    {
        $repositorio = $em->getRepository(Usuario::class);

        $usuarios = $repositorio->findAll();

        return $this->render(
            'Usuarios/list.html.twig',
            [
                'users' => $usuarios
            ]
            );
    }


    /**
     * @Route("/usuarios/nuevo", name="nuevo_usuario")
     */

    public function nuevoUsuario(Request $request, EntityManagerInterface $em)
    {

        $form = $this->createForm(UsuarioFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $usuario = $form->getData();

            $em->persist($usuario);
            $em->flush();


            return $this->redirectToRoute('listar_usuarios');
        }

        return $this->render(
            'Usuarios/form.html.twig',
            [
                'formulario' => $form->createView()

            ]
        );
    }

        /**
     * @Route("/usuarios/edit/{id}", name="editar_usuario")
     */

    public function editarUsuario(Usuario $usuario, Request $request, EntityManagerInterface $em)
    {

        $form = $this->createForm(UsuarioFormType::class, $usuario);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $usuario = $form->getData();

            $em->persist($usuario);
            $em->flush();


            return $this->redirectToRoute('listar_usuarios');
        }

        return $this->render(
            'Usuarios/form.html.twig',
            [
                'formulario' => $form->createView()

            ]
        );
    }

}




