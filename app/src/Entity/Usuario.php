<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioRepository")
 */
class Usuario
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Nombre;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Email;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $Horario;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Ciudad;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ZIP;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->Nombre;
    }

    public function setNombre(string $Nombre): self
    {
        $this->Nombre = $Nombre;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    public function getHorario(): ?string
    {
        return $this->Horario;
    }

    public function setHorario(?string $Horario): self
    {
        $this->Horario = $Horario;

        return $this;
    }

    public function getCiudad(): ?string
    {
        return $this->Ciudad;
    }

    public function setCiudad(string $Ciudad): self
    {
        $this->Ciudad = $Ciudad;

        return $this;
    }

    public function getZIP(): ?int
    {
        return $this->ZIP;
    }

    public function setZIP(?int $ZIP): self
    {
        $this->ZIP = $ZIP;

        return $this;
    }
}
