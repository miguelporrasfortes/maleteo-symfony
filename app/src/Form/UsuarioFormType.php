<?php


namespace App\Form;

use App\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuarioFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('Nombre');
        $builder->add('Email');
        $builder->add('Horario');
        $builder->add('Ciudad', ChoiceType::class, 
            [
                'choices' => [
                    'Madrid' => 'Madrid',
                    'Barcelona' => 'Barcelona',
                    'Valencia' => 'Valencia',
                    'Sevilla' => 'Sevilla'
                ]
            ]
    );
        $builder->add('ZIP');
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Usuario::class
            ]
            );
    }


} 